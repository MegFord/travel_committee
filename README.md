== README ==

=== Setup ===
sudo apt install python3-pip

sudo apt-get install postgresql postgresql-contrib libpq-dev

pip3 install virtualenv

cd travel-committee

virtualenv --python=/usr/bin/python3.6 .

virtualenv env

source env/bin/activate

pip3 install -r requirements.txt

sudo su - postgres

psql

create user travel_user with password 'fake_password';

create database travel_committee with owner travel_user;

python manage.py makemigrations

python manage.py migrate

python manage.py runscript load_csv.py

=== Run ===
python manage.py runserver



