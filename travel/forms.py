from django import forms
from django.core.exceptions import ValidationError
from .models import Application, AirportCode

from crispy_forms.layout import Field

class CustomCheckbox(Field):
    template = 'custom_checkbox.html'

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        localized_fields = ('__all__')
        fields = [
            'user',
            'full_name',
            'foundation_member',
            'conference',
            'hackfest_name',
            'reimbursement_type',
            'employment_affiliation',
            'gnome_intern',
            'event_role',
            'event_start_date',
            'event_end_date',
            'arrival_date',
            'departure_date',
            'depart_airport_code',
            'dest_airport_code',
            'statement',
            'travel_cost',
            'lodging_cost',
            'visa_cost',
            'financial_assistance_cost',
            'own_cost',
            'total_cost',
            'total_requested_cost',
            'risk_agreement',
            'ad_agreement',
            'policy_agreement'
        ]

    risk_agreement = forms.BooleanField(required=True, label='The GNOME Foundation will not reimburse, compensate or indemnify any risk, expense or liability not explicitly identified and agreed to in the sponsorship agreement. Any additional costs you incur or damages you suffer, even unforseen ones, will not be borne by the GNOME Foundation. You are not covered by any insurance policy held by the GNOME Foundation. You are not an employee of the GNOME Foundation.')
    ad_agreement = forms.BooleanField(required=True, label='The GNOME Foundation reserves the right to use information about the sponsorship publicly and use it in any advertisements or promotional reasons seen fit by the GNOME Foundation.')
    policy_agreement = forms.BooleanField(required=True, label='You have read and agree to the conditions of the GNOME Project Travel Sponsorship Policy at https://wiki.gnome.org/Travel/, including the requirement to write a report about the event.')

    def clean_depart_airport_code(self):
        code = self.cleaned_data['depart_airport_code']
        if not AirportCode.objects.filter(iata_code=code):
            raise ValidationError("Please enter a valid three letter IATA code for your departure airport")

        return code

    def clean_dest_airport_code(self):
        code = self.cleaned_data['dest_airport_code']
        if not AirportCode.objects.filter(iata_code=code):
            raise ValidationError("Please enter a valid three letter IATA code for your destination airport")

        return code