from django.db import models


class AirportCode(models.Model):
    airport_name = models.CharField(max_length=255)
    iso_country = models.CharField(max_length=255)
    iata_code= models.CharField(max_length=255)


class Application(models.Model):
    CONFERENCE_CHOICES = [
        ('GUADEC', 'GUADEC'),
        ('GNOMEASIA', 'GNOME.Asia'),
        ('LAS', 'LAS'),
    ]
    REIMBURSEMENT_CHOICES = [
        ('PAYPAL', 'PayPal'),
        ('TRANSFERWISE', 'TransferWise'),
        ('WIRETRANSFER', 'Wire Transfer'),
        ('BANKTRANSFER','Bank Transfer'),
        ('CHECK', 'Check (in USD currency only, from a US bank)'),

    ]
    user = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255, default='')
    conference = models.CharField(
        max_length=8,
        choices=CONFERENCE_CHOICES,
        null=True,
        blank=True,
        db_index=True,
    )
    hackfest_name = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    reimbursement_type = models.CharField(
        max_length=30,
        choices=REIMBURSEMENT_CHOICES,
        null=True,
        blank=True,
    )
    employment_affiliation = models.CharField(max_length=255, default='No Affiliation', blank=True, null=True)
    foundation_member = models.BooleanField()
    gnome_intern = models.BooleanField()
    event_role = models.CharField(max_length=225)
    event_start_date = models.DateField()
    event_end_date = models.DateField()
    arrival_date = models.DateField()
    departure_date = models.DateField()
    depart_airport_code = models.CharField(max_length=3)
    dest_airport_code = models.CharField(max_length=3)
    statement = models.TextField(max_length=150)
    travel_cost = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    lodging_cost = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    visa_cost = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    financial_assistance_cost = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    own_cost = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    total_cost = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    total_requested_cost = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    cost_notes = models.TextField(max_length=100)
    risk_agreement = models.BooleanField()
    ad_agreement = models.BooleanField()
    policy_agreement = models.BooleanField()

