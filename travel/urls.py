from django.urls import path

from .views import ApplicationView, HomeView, LoginView, LogoutView, TokenView

urlpatterns = [
    path('', HomeView.as_view()),
    path('login/', LoginView.as_view()),
    path('logout/', LogoutView.as_view()),
    path('callback/', TokenView.as_view()),
    path('apply/', ApplicationView.as_view()),
]
