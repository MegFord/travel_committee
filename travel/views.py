from django.conf import settings
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.views import View
from gitlab import Gitlab
import decimal
import requests
import secrets
import textwrap

from .forms import ApplicationForm
from .models import Application

def md_checkbox(data):
    return '[{}]'.format('x' if data else ' ')


def md_bool(data):
    return 'YES' if data else 'NO'


def md_currency(data):
    return data.quantize(decimal.Decimal('0.01'), decimal.ROUND_HALF_UP) if data else 0.00

class ApplicationView(View):
    model = Application
    form_class = ApplicationForm
    template_name = 'application.html'

    def get(self, request):
        if request.session['nonce'] != request.GET['state']:
            raise Exception("nonces don't match, security error")

        request.session['nonce'] = None
        token = request.GET['access_token']
        request.session['gitlab_token'] = token

        gitlab = Gitlab(settings.AUTH_SERVER, oauth_token=token)
        gitlab.auth()

        form = ApplicationForm({
            'user': gitlab.user.username,
            'full_name': gitlab.user.name
        })

        return render(request, 'application.html', {'form': form})

    def post(self, request):
        # create a form instance and populate it with data from the request:
        form = ApplicationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            token = request.session['gitlab_token']
            gitlab = Gitlab(settings.AUTH_SERVER, oauth_token=token)
            gitlab.auth()

            d = form.cleaned_data
            event = d['conference'] or d['hackfest_name']
            description = textwrap.dedent(f'''
                ## Personal information

                - **Full name:** {d['full_name']}
                - **Foundation member:** {md_bool(d['foundation_member'])}
                - **Intern:** {md_bool(d['gnome_intern'])}
                - **Affiliation:** {d['employment_affiliation']}

                ## Event

                - **Event:** {event} ({d['event_start_date'].isoformat()}
                    to {d['event_end_date'].isoformat()})
                - **Role at event:** {d['event_role']}
                - **Dates at event:** {d['arrival_date'].isoformat()}
                    to {d['departure_date'].isoformat()}
                - Roundtrip flight from **{d['depart_airport_code']}**
                    to **{d['dest_airport_code']}**

                ## Sponsorship request

                - **Travel:** {md_currency(d['travel_cost'])}
                - **Lodging:** {md_currency(d['lodging_cost'])}
                - **Visa:** {md_currency(d['visa_cost'])}
                - **Other finanical assistance:** {
                    md_currency(d['financial_assistance_cost'])}
                - **Own cost**: –{md_currency(d['own_cost'])}
                - **Total cost:** {md_currency(d['total_cost'])}
                - **Total requested cost:** {
                    md_currency(d['total_requested_cost'])}

                ## Agreements

                - {md_checkbox(d['risk_agreement'])} Risk agreement
                - {md_checkbox(d['ad_agreement'])} Ad agreement
                - {md_checkbox(d['policy_agreement'])} Policy agreement

                ## Statement of interest

                {d['statement']}
            ''')

            project = gitlab.projects.get('MegFord/travel-test')
            issue = project.issues.create({
                'title': 'Sponsorship request for {}'.format(d['full_name']),
                'confidential': True,
                'labels': [event],
                'description': description,
            })
            request.session['issue'] = issue.web_url

            return HttpResponseRedirect('/travel_application/logout/')

        return render(request, 'application.html', {'form': form})

    def home(request):
        return render(request, 'base.html')

class LoginView(View):
    template_name = 'login.html'

    def get(self, request):
        nonce = secrets.token_urlsafe()
        request.session['nonce'] = nonce
        redirect_uri = request.build_absolute_uri('/travel_application/callback/')
        params = {
            'client_id': settings.AUTH_CLIENT_ID,
            'redirect_uri': redirect_uri,
            'response_type': 'token',
            'scope': 'api',
            'state': nonce,
        }
        endpoint = ('{}/oauth/authorize?'.format(settings.AUTH_SERVER) +
                    '&'.join(['{}={}'.format(key, value)
                            for key, value in params.items()]))
        return redirect(endpoint)

class TokenView(View):
    template_name = 'token.html'

    def get(self, request):
        return render(request, 'token.html')

class LogoutView(View):
    template_name = 'logout.html'

    def get(self, request):
        header = {
            'Authorization': 'Bearer ' + request.session['gitlab_token'],
        }
        form = {
            'token': request.session['gitlab_token'],
        }
        requests.post(f'{settings.AUTH_SERVER}/oauth/revoke', headers=header,
                      data=form)

        request.session['nonce'] = None
        request.session['gitlab_token'] = None
        issue_url = request.session['issue']
        request.session['issue'] = None
        return redirect(issue_url)

class HomeView(View):
    template_name = 'base.html'

    def get(self, request):
        return render(request, 'base.html')
